$(document).ready(function() {
	var menuRow = d3.select("#menu-row");

	resizeBackgroundVideo();

	// resize video header
	$(window).resize(function() {
		resizeBackgroundVideo();
	});

	// Enable map zooming with mouse scroll when the user clicks the map
	$('.maps.embed-container').on('click', onMapClickHandler);

	// menu on scroll
	$(document).scroll(function() {
		var links = $("#menu").find('ul li a');
		if ($(document).scrollTop() >= 400) {
			$("#menu-row").removeClass("large").addClass("small").fadeIn("slow");
			$("#menu-wrapper").removeClass("large").addClass("small").fadeIn("slow");
			$("#logo").removeClass("large").addClass("small").fadeIn("slow");
			$("#phone").removeClass("large").addClass("small").fadeIn("slow");
			$("#phone-icon").removeClass("large").addClass("small").fadeIn("slow");
			links.removeClass("large").addClass("small").fadeIn("slow");
		} else{
			$("#menu-row").removeClass("small").addClass("large").fadeIn("slow");
			$("#menu-wrapper").removeClass("small").addClass("large").fadeIn("slow");
			$("#logo").removeClass("small").addClass("large").fadeIn("slow");
			$("#phone").removeClass("small").addClass("large").fadeIn("slow");
			$("#phone-icon").removeClass("small").addClass("large").fadeIn("slow");
			links.removeClass("small").addClass("large").fadeIn("slow");
		}
	});

	function scrollTween(start, offset) {
		return function() {
			var i = d3.interpolateNumber(start, offset);
			return function(t) { scrollTo(0, i(t)); };
		};
	}

	var animating = false;
	d3.selectAll("#menu>ul>li>a").on('click', function() {
		var sectionSelector = $(this).attr("href");
		var htmlBody = $('html, body');
		var distance = Math.abs(htmlBody.scrollTop() - $(sectionSelector).offset().top);
		var maxDistance = htmlBody.height();
		var speed = maxDistance / 3000;


		var clickPosition = d3.mouse(menuRow.node());
		clickPosition.x = clickPosition[0];
		clickPosition.y = clickPosition[1];
		var circleRadius = menuRow.node().clientHeight;


		if(!animating) {
			animating = true;
			var svg = menuRow.append("svg")
				.style("position", "absolute")
				.style("left", clickPosition.x - circleRadius)
				.style("top", clickPosition.y - circleRadius);
			var g = svg.append("g")
				.attr("transform", "translate(" + circleRadius + "," + circleRadius + ")");
			var circle = g.append("circle")
				.attr("r", 10)
				.attr("fill", "#0071bb")
				.style("opacity", 0.2);

			circle.transition().duration(400)//.ease("bounce")
				.attr("r", circleRadius)
				.each("end", function() {
					svg.remove();
					animating = false
				});
		}


		var scrollOffset = d3.select(sectionSelector).node().offsetTop - d3.select("body").node().offsetTop - menuRow.node().clientHeight;

		d3.transition()
			.duration(distance / speed)
			.tween("scroll", scrollTween(window.pageYOffset, scrollOffset));

		d3.event.stopPropagation();
	}).on('touchstart', function() {
		var sectionSelector = $(this).attr("href");
		var htmlBody = $('html, body');
		var distance = Math.abs(htmlBody.scrollTop() - $(sectionSelector).offset().top);
		var maxDistance = htmlBody.height();
		var speed = maxDistance / 3000;

		var touchPosition = d3.touches(menuRow.node())[0];
		touchPosition.x = touchPosition[0];
		touchPosition.y = touchPosition[1];

		var circleRadius = menuRow.node().clientHeight;

		if(!animating) {
			animating = true;
			var svg = menuRow.append("svg")
				.style("position", "absolute")
				.style("left", touchPosition.x - circleRadius)
				.style("top", touchPosition.y - circleRadius);
			var g = svg.append("g")
				.attr("transform", "translate(" + circleRadius + "," + circleRadius + ")");
			var circle = g.append("circle")
				.attr("r", 10)
				.attr("fill", "#0071bb")
				.style("opacity", 0.2);

			circle.transition().duration(400)
				.attr("r", circleRadius);

			d3.event.preventDefault();
			d3.select(d3.event.target)
				.on("touchend", function() {
					svg.remove();
					animating = false;

					var scrollOffset = d3.select(sectionSelector).node().offsetTop - d3.select("body").node().offsetTop - menuRow.node().clientHeight;

					d3.transition()
						.duration(distance / speed)
						.tween("scroll", scrollTween(window.pageYOffset, scrollOffset));
				});
		}
	});

	var logo = d3.select("#logo");

	logo.on('click', function() {
		var clickPosition = d3.mouse(this);
		clickPosition.x = clickPosition[0];
		clickPosition.y = clickPosition[1];
		var circleRadius = menuRow.node().clientHeight;


		if(!animating) {
			animating = true;
			var svg = menuRow.append("svg")
				.style("position", "absolute")
				.style("left", clickPosition.x - circleRadius)
				.style("top", clickPosition.y - circleRadius);
			var g = svg.append("g")
				.attr("transform", "translate(" + circleRadius + "," + circleRadius + ")");
			var circle = g.append("circle")
				.attr("r", 10)
				.attr("fill", "#333")
				.style("opacity", 0.2);

			circle.transition().duration(400)//.ease("bounce")
				.attr("r", circleRadius)
				.each("end", function() {
					svg.remove();
					animating = false
				});
		}

		if(window.pageYOffset != 0) {
			var distance = Math.abs(window.pageYOffset);
			var maxDistance = d3.select("body").node().clientHeight;
			var speed = maxDistance / 3000;

			d3.transition()
				.duration(distance / speed)
				.tween("scroll", scrollTween(window.pageYOffset, -window.pageYOffset));
		}
	}).on('touchstart', function() {
		var distance = Math.abs(window.pageYOffset);
		var maxDistance = d3.select("body").node().clientHeight;
		var speed = maxDistance / 3000;

		var touchPosition = d3.touches(menuRow.node())[0];
		touchPosition.x = touchPosition[0];
		touchPosition.y = touchPosition[1];

		var circleRadius = menuRow.node().clientHeight;

		if(!animating) {
			animating = true;
			var svg = menuRow.append("svg")
				.style("position", "absolute")
				.style("left", touchPosition.x - circleRadius)
				.style("top", touchPosition.y - circleRadius);
			var g = svg.append("g")
				.attr("transform", "translate(" + circleRadius + "," + circleRadius + ")");
			var circle = g.append("circle")
				.attr("r", 10)
				.attr("fill", "#333")
				.style("opacity", 0.2);

			circle.transition().duration(400)
				.attr("r", circleRadius);

			d3.event.preventDefault();
			d3.select(d3.event.target)
				.on("touchend", function() {
					svg.remove();
					animating = false;

					d3.transition()
						.duration(distance / speed)
						.tween("scroll", scrollTween(window.pageYOffset, -window.pageYOffset));
				});
		}
	});

	// Check iOS for Youtube functionalities
	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
	if(iOS) {
		d3.select("#header").style("background-image", "url(../images/header.jpg)");
		d3.select(".video-container").style("display", "none");
	}
});

function resizeBackgroundVideo() {
	var ratio = 16/9;
	var wvideo = $(window).width();
	var hvideocontainer = $('#header').height();
	var videoWrapper = $(".video-wrapper");

	if (wvideo/hvideocontainer >= ratio) {
		videoWrapper.height(wvideo / ratio);
		videoWrapper.width(wvideo);

	} else{
		videoWrapper.height(hvideocontainer);
		videoWrapper.width(hvideocontainer * ratio);
	}
	videoWrapper.css("margin-top", -(videoWrapper.height()/2 - hvideocontainer/2) );
	videoWrapper.css("margin-left", -(videoWrapper.width()/2 - wvideo/2) );
}

// Disable scroll zooming and bind back the click event
var onMapMouseleaveHandler = function (event) {
	var that = $(this);

	that.on('click', onMapClickHandler);
	that.off('mouseleave', onMapMouseleaveHandler);
	that.find('iframe').css("pointer-events", "none");
};

var onMapClickHandler = function (event) {
	var that = $(this);

	// Disable the click handler until the user leaves the map area
	that.off('click', onMapClickHandler);

	// Enable scrolling zoom
	that.find('iframe').css("pointer-events", "auto");

	// Handle the mouse leave event
	that.on('mouseleave', onMapMouseleaveHandler);
};


