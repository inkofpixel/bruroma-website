var express = require('express');
var router = express.Router();
var accepts = require('accepts');

/* GET home page. */
router.get('/', function(req, res, next) {
	var accept = accepts(req);
	var preferredLanguage = accept.languages()[0];

	console.log(preferredLanguage);
	if(preferredLanguage) {
		switch(preferredLanguage) {
			case "it-IT":case "it-it":case "it":
				res.render('it/index', { title: "Bruroma" });
				break;
			default:
				res.render('index', { title: 'Bruroma' });
		}
	} else {
		res.render('index', { title: 'Bruroma' });
	}
});

module.exports = router;
